package ExtUtils::XBuilder;

use strict;
use warnings FATAL => 'all';

use vars qw($VERSION @ISA %OSTYPES);

require File::Spec;

=head1 NAME

ExtUtils::XBuilder - Compile an link external code

=cut

$VERSION = '0.001';

=head1 SYNOPSIS

    use ExtUtils::XBuilder;

    my $foo = ExtUtils::XBuilder->new( lang => "C" );
    my $bar = ExtUtils::XBuilder->new( lang => "C++" );

    $foo->compile( "file1.c" );
    $foo->compile( "module.xs" );
    $bar->compile( "api.cc" );
    $bar->compile( "extend.cc" );
    $bar->link( "api.o", "extend.o", "file1.o", "module.o" );

=head1 DESCRIPTION

Blubber

=cut

# OS types like ExtUtils::CBuilder from perlport.pod

%OSTYPES = qw(
  aix          Unix
  bsdos        Unix
  darwin       Unix
  dgux         Unix
  dynixptx     Unix
  freebsd      Unix
  haiku        Unix
  linux        Unix
  hpux         Unix
  irix         Unix
  next         Unix
  openbsd      Unix
  dec_osf      Unix
  svr4         Unix
  sco_sv       Unix
  unicos       Unix
  unicosmk     Unix
  solaris      Unix
  sunos        Unix
  netbsd       Unix
  dragonflybsd Unix
  gnukfreebsd  Unix

  dos          DOSish
  MSWin32      DOSish
  os2          DOSish

  os390        EBCDIC
  os400        EBCDIC
  posix-bc     EBCDIC
  os390        EBCDIC
  );

# We only use this once - don't waste a symbol table entry on it.
# More importantly, don't make it an inheritable method.
my $load = sub {
    my $mod = shift;
    eval "use $mod";
    die $@ if $@;
    # XXX fix to use roles (see S::S)
    @ISA = ($mod);
};

{
    my @package = split /::/, __PACKAGE__;

    if ( grep { -e File::Spec->catfile( $_, @package, 'Platform', $^O ) . '.pm' } @INC )
    {
        $load->( __PACKAGE__ . "::Platform::$^O" );

    }
    elsif ( exists $OSTYPES{$^O}
            and grep { -e File::Spec->catfile( $_, @package, 'Platform', $OSTYPES{$^O} ) . '.pm' }
            @INC )
    {
        $load->( __PACKAGE__ . "::Platform::$OSTYPES{$^O}" );

    }
    else
    {
        $load->( __PACKAGE__ . "::Base" );
    }
}

=head1 AUTHOR

Jens Rehsack, C<< <rehsack at cpan.org> >>

=head1 BUGS

Please report any bugs or feature requests to
C<bug-extutils-xbuilder at rt.cpan.org>, or through the web interface at
L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=ExtUtils-XBuilder>.
I will be notified, and then you'll automatically be notified of progress
on your bug as I make changes.

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc ExtUtils::XBuilder

You can also look for information at:

=over 4

=item * RT: CPAN's request tracker

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=ExtUtils-XBuilder>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/ExtUtils-XBuilder>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/ExtUtils-XBuilder>

=item * Search CPAN

L<http://search.cpan.org/dist/ExtUtils-XBuilder/>

=back

=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2011 Jens Rehsack.

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See http://dev.perl.org/licenses/ for more information.

=cut

1;    # End of ExtUtils::XBuilder
