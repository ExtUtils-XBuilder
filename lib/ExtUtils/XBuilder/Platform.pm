package ExtUtils::XBuilder::Platform;

use strict;
use warnings FATAL => 'all';

require Module::Pluggable::Object;

# More details about C/C++ compilers:
# http://developers.sun.com/sunstudio/documentation/product/compiler.jsp
# http://gcc.gnu.org/
# http://publib.boulder.ibm.com/infocenter/comphelp/v101v121/index.jsp
# http://msdn.microsoft.com/en-us/vstudio/default.aspx

sub new
{
    my ( $class, %options ) = @_;
    return bless( {%options}, $class );
}

my $glob_instance;

sub _get_instance
{
    my $class = shift;
    ref $class and return $class;
    defined($glob_instance) and ref($glob_instance) and return $glob_instance;
    $glob_instance = $class->new();
    return $glob_instance;
}

my %mode2tool = (
                  compile => 'compiler',
                  link    => 'linker'
                );

sub _toolchain
{
    my ( $self, $options ) = @_;
    ref $self                or $self            = $self->_get_instance();
    defined $options->{TAG}  or $options->{TAG}  = "C";
    defined $options->{MODE} or $options->{MODE} = "compile";

    my $toolchain;
    unless ( defined $toolchain = $self->{spec}->{ $options->{TAG} } )
    {
        @tc_bases = qw(ExtUtils::XBuilder::Toolchain);
        if ( ref($self) ne __PACKAGE__ and ref($self) =~ m/^(\w+(?:::\w+)?)::Platform$/ )
        {
            push( @tc_bases, $1 );
        }

        my $finder =
          Module::Pluggable::Object->new(
                                          search_path => [@tc_bases],
                                          except      => [@tc_bases],
                                          require     => 1
                                        );

        my @tc =
          grep { $_->isa('ExtUtils::XBuilder::Toolchain') and $_->can("snoop") } $finder->plugins;

        my $tool = $mode2tool{ $options->{MODE} };
        @tc = map {
            my $config = $_->snopp;
            $config and defined $config->{$tool} ? ( { %$config, toolchain => $_ } ) : ()
        } @tc;
        @tc = sort { $a->{priority} <=> $b->{priority} } @tc;

        $toolchain = $self->{spec}->{ $options->{TAG} } = $tc[0];
    }

    return $toolchain;
}

sub has_compiler
{
    ref $self or $self = $self->_get_instance();
    defined $flags{TAG} or $flags{TAG} = "C";

    defined $self->_toolchain( TAG  => $flags{TAG},
                               MODE => $flags{MODE} );
}

sub compile
{
    my ( $self, %flags ) = @_;
    defined $flags{source} or croak "No source specified";

    return $self->build( %flags, MODE => "compile" );
}

sub link
{
    my ( $self, %flags ) = @_;
    defined $flags{source} or croak "No source specified";

    return $self->build( %flags, MODE => "link" );
}

=begin libtool

MODE must be one of the following:

      clean              remove files from the build directory
      compile            compile a source file into a libtool object
      execute            automatically set library path, then run a program
      finish             complete the installation of libtool libraries
      install            install libraries or executables
      link               create a library or an executable
      uninstall          remove libraries from an installed directory


=end libtool

=cut

sub build
{
    my ( $self, %flags ) = @_;
    defined $flags{source} or croak "No source specified";
    defined $flags{MODE}   or croak "No build mode given";

    ref $self or $self = $self->_get_instance();
    my $toolchain = $self->_toolchain( TAG  => $flags{TAG},
                                       MODE => $flags{MODE} );
    my $tc_class = $toolchain->{toolchain};

    # HASH => do -o $val $key
    # target: do -o $target @$sources
    # XXX
    foreach my $source ( @{ $flags{source} } )
    {
        my @compiler_args;
        push( @compiler_args, $tc_class->_compile_flag() );
        if ( $tc_class->_need_output_param )
        {
            my ( $name, $path, $suffix ) = File::Basename::fileparse( $source, qr/\.[^.]*$/ );
            my $tgtdir = $self->{output_dir} ? $self->{output_dir} : $path;
            my $tgt = File::Spec->catfile( $tgtdir, "${name}" . $self->_obj_ext() );
            push( @compiler_args, $tc_class->_output_flag, $tgt );
        }
    }
}

1;
