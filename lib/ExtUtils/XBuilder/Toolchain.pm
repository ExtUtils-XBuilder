package ExtUtils::XBuilder::Toolchain;

use strict;
use warnings FATAL => 'all';

use Carp qw/croak/;
use IO::CaptureOutput qw(capture capture_exec);
require IPC::Cmd;

sub new
{
    my ($class) = @_;
    return bless( { priority => 0 }, $class );
}

sub USER_PRIO        { 10 }
sub PERL_CONFIG_PRIO { 20 }
sub NATIVE_PRIO      { 30 }
sub THIRD_PARTY_PRIO { 40 }

sub language_order { qw(c++ f95 f77 c) }

my %env_names = (
                  C => {
                         CC        => 'compiler',
                         CFLAGS    => 'compile_flags',
                         CCLD      => 'linker',
                         CCLDFLAGS => 'link_flags',
                       },
                  CXX => {
                           CXX        => 'compiler',
                           CXXFLAGS   => 'compile_flags',
                           CXXLD      => 'linker',
                           CXXLDFLAGS => 'link_flags',
                         },
                  '' => {
                          AR        => 'archiver',
                          NM        => 'nm',
                          LD        => 'linker',
                          LDFLAGS   => 'link_flags',
                          LDDLFLAGS => 'dyn_link_flags',
                        },
                );

sub _version_info_flag  { '' }
sub _version_info_match { 0 }

sub _warning_flag      { '' }
sub _warn_error_flag   { '' }
sub _warn_noerror_flag { '' }

sub _is_native { 0 }

sub _env_names
{
    my ( $self, $options, $more ) = @_;
    if ( defined($more) and "HASH" eq ref($mode) )
    {
        defined $env_names{ $options->{TAG} }
          and croak("environment setting for tag '$options->{TAG}' already defined");
        $env_names{ $options->{TAG} } = [ @{$more} ];
    }

    return defined $env_names{ $options->{TAG} } ? $env_names{ $options->{TAG} } : {};
}

sub _snoop_env
{
    my ( $class, $options ) = @_;
    my $env_names = $class->_env_names($options);
    my $config    = {};

    foreach my $env_key ( keys %{$env_names} )
    {
        defined $ENV{$env_key} or next;
        $config->{ $env_names->{$env_key} } = $ENV{$env_key};
        $config->{priority} = USER_PRIO;
    }

    defined $config->{priority} or return;
    return $config;
}

my %Config_names = (
                     C => {
                            cc      => 'compiler',
                            ccflags => 'compile_flags'
                          },
                     '' => {
                             ar      => 'archiver',
                             nm      => 'nm',
                             ld      => 'linker',
                             ldflags => 'link_flags'
                           },
                   );

sub _snoop_Config
{
    my ( $class, $options ) = @_;

    defined $Config_names->{ $options->{TAG} } or return;
    my $Config_names = $Config_names{ $options->{TAG} };
    my $config       = {};

    foreach my $cfg_key ( keys %{$Config_names} )
    {
        defined $Config{$cfg_key} or next;
        $config->{ $Config_names->{$cfg_key} } = $Config{$cfg_key};
        $config->{priority} = USER_PRIO;
    }

    defined $config->{priority} or return;
    return $config;
}

sub _extra_paths { [] }

sub _snoop_Path
{
    my ( $class, $options ) = @_;

    defined $Config_names->{ $options->{TAG} } or return;
    my $config;

    my $compilers = $self->_compiler_executables($options);
    ref($compilers) eq "ARRAY" or return;

    foreach my $compiler (@$compilers)
    {
        my $compiler_path = IPC::Cmd::can_run($compiler) or next;
        $config = {
                    compiler => $compiler,
                    priority => $class->_is_native($compiler_path) ? NATIVE_PRIO : THIRD_PARTY_PRIO
                  };
    }

    defined $config->{priority} or return;
    return $config;
}

sub _snoop_Guess
{
    my ( $class, $options ) = @_;

    defined $Config_names->{ $options->{TAG} } or return;

    $options->{TAG} eq 'C' and return;
    $options->{TAG} eq ''  and return;

    my $ccfg = $class->snoop( { %options, TAG => "C" } );
    $ccfg or return;

    my $config;

    defined $config->{priority} or return;
    return $config;
}

sub _verify_compiler
{
    my ( $class, $config ) = @_;

    if ( defined $config->{compiler} )
    {
        ( $stdout, $stderr, $success, $exit_code ) =
          capture_exec( $config->{compiler}, $class->_version_info_flag );
              $success
          and $class->_version_info_match($stderr)
          and --$config->{priority}
          and return $config;
    }

    return;
}

sub compile
{
    my ( $self, $file, $options ) = @_;
    1;
}

sub link
{
}

sub preprocess
{
}

1;
