package ExtUtils::XBuilder::Platform::Unix;

use strict;
use warnings FATAL => 'all';

use parent qw(ExtUtils::XBuilder::Platform);

sub _obj_ext    { '.o' }
sub _lib_ext    { '.a' }
sub _dynlib_ext { '.so' }
sub _exe_ext    { '' }
sub _def_ext    { '' }
sub _map_ext    { '.map' }

1;
