package ExtUtils::XBuilder::Toolchain::SunPRO;

use strict;
use warnings FATAL => 'all';

use parent qw(ExtUtils::XBuilder::Toolchain);

sub _extra_paths
{
    [
       join(
             ":",
             (
                defined $ENV{SUNWSPROBASE}
                ? ( File::Spec->catdir( $ENV{SUNWSPROBASE}, 'bin' ) )
                : ()
             )
           )
    ];
}

1;
