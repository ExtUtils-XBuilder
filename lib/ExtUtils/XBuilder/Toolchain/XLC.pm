package ExtUtils::XBuilder::Toolchain::XLC;

use strict;
use warnings FATAL => 'all';

use parent qw(ExtUtils::XBuilder::Toolchain);

sub _extra_paths { ['/usr/vac/bin:/usr/vacpp/bin'] }

1;
