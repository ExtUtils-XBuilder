package ExtUtils::XBuilder::Toolchain::cc;

use strict;
use warnings FATAL => 'all';

use parent qw(ExtUtils::XBuilder::Toolchain);

use Config;

sub snoop
{
    my ( $class, $options ) = @_;
    defined $options->{TAG} or $options->{TAG} = "C";

    my $config;

    $config = $class->_snoop_env($options)    and return $config;
    $config = $class->_snoop_Config($options) and return $config;

    return;
}

sub _compile_flag { '-c' }
sub _output_flag  { '-o' }

sub _need_output_param { 1 }

my %default_compiler_executables = (
                                     C       => [qw(cc)],
                                     CXX     => [qw(++ c++ CC)],
                                     FORTRAN => [qw(f95 f77 f2c)],
                                     ''      => [qw(ld)],
                                   );

sub _compiler_executables
{
    my ( $class, $options ) = @_;
    return $default_compiler_executables{ $options->{TAG} };
}

=begin private

sub toolchain_init
{
    my $self = $_[0];

    $self->{supported_languages} = [qw(cc cxx f95)];

    # XXX restrict to interesting keys ...
    while ( my ( $k, $v ) = each %Config )
    {
        $self->{config}{$k} = $v unless exists $self->{config}{$k};
    }

    unless ( exists $self->{config}{cxx} )
    {
        my ( $ccpath, $ccbase, $ccsfx ) = fileparse( $self->{config}{cc}, qr/\.[^.]*/ );
        foreach my $cxx ( @{ $cc2cxx{$ccbase} } )
        {
            if ( can_run( File::Spec->catfile( $ccpath, $cxx, $ccsfx ) ) )
            {
                $self->{config}{cxx} = File::Spec->catfile( $ccpath, $cxx, $ccsfx );
                last;
            }
            if ( can_run( File::Spec->catfile( $cxx, $ccsfx ) ) )
            {
                $self->{config}{cxx} = File::Spec->catfile( $cxx, $ccsfx );
                last;
            }
            if ( can_run($cxx) )
            {
                $self->{config}{cxx} = $cxx;
                last;
            }
        }
        unless ( exists $self->{config}{cxx} )
        {
            $self->{config}{cxx} = $self->{config}{cc};
            my $cflags = $self->{config}{cflags};
            $self->{config}{cxxflags} = '-x c++';
            $self->{config}{cxxflags} .= " $cflags" if defined $cflags;
        }
    }

}

sub priority { $_[0]->{config}->{priority} }

sub toolchain_match
{
    return 1;
}

sub guess_compiler_for
{
    my ( $self, $lang ) = @_;
}

=end private

=cut

1;
