package ExtUtils::XBuilder::Toolchain::GCC;

use strict;
use warnings FATAL => 'all';

use parent qw(ExtUtils::XBuilder::Toolchain);

require File::Spec;
require File::Basename;
require IPC::Cmd;

# More details about GNU C/C++ compilers:
# http://gcc.gnu.org/

sub _verify_compiler
{
    my ( $class, $config ) = @_;

    my $verified = $class->SUPER::_verify_compiler($config);
    $verified and return $verified;

    # XXX likely to find gxlc (gcc compat cli to xlc)
    defined $config->{compiler}
      and File::Basename::basename( $config->{compiler} ) =~ m/^g..$/
      and --$config->{priority}
      and return $config;

    return;
}

sub snoop
{
    my ( $class, $options ) = @_;
    defined $options->{TAG} or $options->{TAG} = "C";

    my $config;

    $config = $class->_verify_compiler( $class->_snoop_env($options) )    and return $config;
    $config = $class->_verify_compiler( $class->_snoop_Config($options) ) and return $config;
    $config = $class->_verify_compiler( $class->_snoop_Path($options) )   and return $config;
    $config = $class->_verify_compiler( $class->_snoop_Guess($options) )  and return $config;

    return;
}

my %default_compiler_executables = (
                                     C       => [qw(gcc cc)],
                                     CXX     => [qw(g++ c++ CC)],
                                     FORTRAN => [qw(gfortran f95 f77)],
                                     ''      => [qw(ld)],
                                   );

sub _compiler_executables
{
    my ( $class, $options ) = @_;
    return $default_compiler_executables{ $options->{TAG} };
}

sub _is_native
{
    my ( $class, $comp_exe_path ) = @_;
    my ( $name, $path, $suffix ) = File::Basename::fileparse( $comp_exe_path, qr/\.[^.]*/ );
    $path eq "/usr/bin" and return 1;
    return 0;
}

sub _version_info_flag { '-v' }
sub _version_info_match { $_[1] =~ m/^gcc/ms }

sub _warning_flag      { '-Wall -Wextra' }
sub _warn_error_flag   { '-Werror' }
sub _warn_noerror_flag { '-Wno-error' }

1;
