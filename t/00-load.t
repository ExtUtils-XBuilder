#!perl -T

use Test::More tests => 1;

BEGIN {
    use_ok( 'ExtUtils::XBuilder' ) || print "Bail out!
";
}

diag( "Testing ExtUtils::XBuilder $ExtUtils::XBuilder::VERSION, Perl $], $^X" );
