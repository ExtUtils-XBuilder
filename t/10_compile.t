#!perl -w

use strict;
use warnings FATAL => 'all';

use Cwd;
use File::Spec;
use File::Basename;

use Test::More;

my $dist_basedir =
  Cwd::abs_path( File::Spec->catdir( File::Basename::dirname($0), File::Spec->updir() ) );

my $lib_pl = File::Spec->catfile( $dist_basedir, "t", "lib.pl" );
do $lib_pl;

my $test_dir = test_dir();
my $test_data = test_data();

use_ok('ExtUtils::XBuilder');

my $builder = ExtUtils::XBuilder->new( output_dir => $test_dir );

SKIP: {
    $builder->has_compiler( TAG => 'C' ) or skip "No C Compiler", 2;

    my @csources = map { File::Spec->catfile( $test_data, $_ ) } qw(hello_world.c);
    my @objects = $builder->compile( source => [ @sources ], TAG => 'C' );
    cmp_ok( scalar(@sources), '==', scalar(@objects), 'compiling hello_world.c' );
    like( $objects[0], qr/^$test_dir/, 'object directory prefix correct' );
    my @executables = $builder->link( source => [ @objects ], TAG => 'C', target => 'hello_world' );
    ...
}

SKIP: {
    $builder->has_compiler( TAG => 'CXX' ) or skip "No C++ Compiler", 2;

    my @csources = map { File::Spec->catfile( $test_data, $_ ) } qw(hello_world.cc);
    my @objects = $builder->compile( source => [ @sources ], TAG => 'CXX' );
    cmp_ok( scalar(@sources), '==', scalar(@objects), 'compiling hello_world.cc' );
    like( $objects[0], qr/^$test_dir/, 'object directory prefix correct' );
    my @executables = $builder->link( source => [ @objects ], TAG => 'CXX', target => 'hello_world' );
    ...
}
