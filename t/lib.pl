#!/usr/bin/perl

# lib.pl is the file where database specific things should live,
# whereever possible. For example, you define certain constants
# here and the like.

use strict;
use warnings FATAL => 'all';

use Cwd;
use File::Basename;
use File::Path;
use File::Slurp;
use File::Spec;
use File::Temp;
use JSON;

my $test_dir;
END { defined( $test_dir ) and rmtree $test_dir }

sub test_dir
{
    unless( defined( $test_dir ) )
    {
	$test_dir = File::Spec->rel2abs( File::Spec->curdir () );
	$test_dir = File::Spec->catdir ( $test_dir, "test_output_" . $$ );
	$^O eq 'VMS' and $test_dir = VMS::Filespec::unixify($test_dir);
	rmtree $test_dir;
	mkpath $test_dir;
    }

    return $test_dir;
}

my $test_data;

sub test_data
{
    unless( defined( $test_data ) )
    {
	my $dist_basedir = Cwd::abs_path( File::Spec->catdir( File::Basename::dirname($0), File::Spec->updir() ) );
	$test_data = File::Spec->catdir( $dist_basedir, 'test_data' );
	$^O eq 'VMS' and $test_data = VMS::Filespec::unixify($test_data);
    }

    return $test_data;
}

1;
